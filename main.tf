provider "aws" {
  region     = "${var.aws_region}"
  //profile = "Acklen"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "s3" {
    region = "us-east-1"
  }

}


module "bucket1" {
  source          = "./modules/s3"
  DevOpsName      = var.DevOpsName
  bucket_name     = "${var.terraform_prefix}${var.bucket_name}"
  domain_name     = "${var.terraform_prefix}${var.domain_name}"
}


module "cloudfront" {
  source                         = "./modules/cloudfront"
  s3_buckets                    = [
                                    {domain_name = module.bucket1.s3_frontend, origin_id = "${var.terraform_prefix}${var.bucket_name}", path_pattern = "/bucket1"}
                                    
                                  ]
  acm_certificate_arn     = var.acm_certificate_arn
  domain_names              = ["${var.terraform_prefix}${var.domain_name}"]
  DevOpsName                    = var.DevOpsName
}

module "route53-bucket1" {
  source                               = "./modules/route53"
  domain_name                          = "${var.terraform_prefix}${var.domain_name}"
  resource_domain_name                 = module.cloudfront.s3_distribution.domain_name
  resource_hosted_zone                 = module.cloudfront.s3_distribution.hosted_zone_id
  route53_zone_id                      = var.route53_zone_id
}




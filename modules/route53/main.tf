resource "aws_route53_record" "root-a" {
  zone_id = var.route53_zone_id
  name    = var.domain_name
  type    = "A"

  alias {
    name                   = var.resource_domain_name
    zone_id                = var.resource_hosted_zone
    evaluate_target_health = false
  }

  depends_on = [var.resource_domain_name, var.resource_hosted_zone]
}